import React from 'react';
import { Link, Router, Route } from "react-router-dom";
import { Layout, Menu, Breadcrumb } from 'antd';
import { PlaySquareTwoTone, BugTwoTone } from '@ant-design/icons';
import 'antd/dist/antd.css';
import Routes from './router'


const { SubMenu } = Menu;
const { Header, Content, Sider, Footer } = Layout;

const Nav = () => {
    return (
        <>  
            <Layout>
                <Header className="header" style={{background:"#177ddc"}}>
                <Menu style={{float: 'right'}} mode="horizontal" defaultSelectedKeys={['1']}>
                    <Menu.Item key="1"><Link to="/">Home</Link></Menu.Item>
                    <Menu.Item key="2"><Link to="/login">Login</Link></Menu.Item>
                    <Menu.Item key="3"><Link to="/Register">Register</Link></Menu.Item>
                </Menu>
                </Header>
                <Layout>
                <Sider width={170} className="site-layout-background">
                    <Menu
                    mode="inline"
                    defaultSelectedKeys={['1']}
                    defaultOpenKeys={['sub2']}
                    style={{ height: '100%', borderRight: 0 }}
                    >
                    <SubMenu key="sub1" icon={<PlaySquareTwoTone />} title="MOVIE">
                        <Menu.Item key="1"><Link to="/movieReviewList">Movie Review</Link></Menu.Item>
                        <Menu.Item key="2"><Link to="/movieTable">Movie Table</Link></Menu.Item>
                        <Menu.Item key="3"><Link to="/movieCreate">Movie Create</Link></Menu.Item>                                                                      
                    </SubMenu>
                    <SubMenu key="sub2" icon={<BugTwoTone />} title="GAME">
                        <Menu.Item key="4"><Link to="/gameReviewList">Game Review</Link></Menu.Item>
                        <Menu.Item key="5"><Link to="/gameTable">Game Table</Link></Menu.Item>
                        <Menu.Item key="6"><Link to="/gameCreate">Game Create</Link></Menu.Item>
                        <Menu.Item key="7"></Menu.Item>
                        <Menu.Item key="8"></Menu.Item>
                        <Menu.Item key="9"></Menu.Item>
                        <Menu.Item key="10"></Menu.Item>
                        <Menu.Item key="11"></Menu.Item>
                        <Menu.Item key="12"></Menu.Item>
                        <Menu.Item key="13"></Menu.Item>
                        <Menu.Item key="14"></Menu.Item>
                        <Menu.Item key="15"></Menu.Item>
                        
                    </SubMenu>
                    </Menu>
                </Sider>
                <Layout style={{ padding: '0 24px 24px' }}>
                    <Content
                    className="site-layout-background"
                    style={{
                        padding: 24,
                        margin: 0,
                        minHeight: 280,
                    }}
                    >
                        <Routes />
                    </Content>
                </Layout>
                </Layout>
                <Layout>
                    <Footer style={{background: "gray", textAlign:"center", fontWeight:"bolder", color:"white"}} >Final Project SanberCode 2020</Footer>
                </Layout>
            </Layout>
        </>
    )
}

export default Nav
