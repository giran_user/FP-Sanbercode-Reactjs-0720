import React from 'react'
import Login from "./pages/login";
import Resgiter from "./pages/register"
import Home from "./pages/movieReviewList"
import MovieReviewList from "./pages/movieReviewList"
import MovieTable from "./pages/movieTable"
import MovieCreate from "./pages/movieCreate"

import GameReviewList from "./pages/gameReviewList"
import GameTable from "./pages/gameTable"
import GameCreate from "./pages/gameCreate"

import MovieDetail from "./pages/movieDetail"
import GameDetail from "./pages/gameDetail"

import { Switch, Route } from "react-router";

import EditDataMovie from './pages/editDataMovie';
import EditDataGame from "./pages/editDataGame";

const Routes = () => {
    return (
        <>
            <Switch>
                <Route exact path="/">
                    <Home />
                </Route>   
                <Route path="/login">
                    <Login />
                </Route>  
                <Route path="/register">
                    <Resgiter />                
                </Route>  
                <Route path="/movieReviewList">
                    <MovieReviewList />                
                </Route>
                <Route path="/movieTable">
                    <MovieTable />                
                </Route>
                <Route path="/movieCreate">
                    <MovieCreate />                
                </Route> 
                <Route path="/gameReviewList">
                    <GameReviewList />                
                </Route>
                <Route path="/gameTable">
                    <GameTable />                
                </Route>
                <Route path="/gameCreate">
                    <GameCreate />                
                </Route>

                <Route path="/movieDetail">
                    <MovieDetail />                
                </Route>

                <Route path="/gameDetail">
                    <GameDetail />                
                </Route>

                <Route path="/editDataMovie">
                    <EditDataMovie  />                
                </Route>  

                <Route path="/editDataGame">
                    <EditDataGame />
                </Route>


            </Switch>            
        </>
    )
}

export default Routes
