import React, { Component } from 'react';
import Axios from 'axios';
import Swal from 'sweetalert2';
import Loading from './loading'

class editDataMovie extends Component {
    state = {
        dataMovie : []
    }

    componentDidMount = () =>{
        this.getDataMovie()
    }

    getDataMovie = () => {
        var id = window.location.pathname.split('/')[2]
        Axios.get(`https://backendexample.sanbersy.com/api/movies/` + id)
        .then((res)=>{
            this.setState({dataMovie : res.data})
            console.log('dapat data')
            console.log(res.data)

        })

        .catch((err)=>{
            console.log(err)

        })
    }

    onSaveEditBtnClick = () =>{
        
        var input_Title = this.refs.inputTitle.value
        var input_Description = this.refs.inputDescription.value
        var input_Year = this.refs.inputYear.value
        var input_Duration = this.refs.inputDuration.value
        var input_Genre = this.refs.inputGenre.value
        var input_Rating = this.refs.inputRating.value
        var input_Review = this.refs.inputReview.value
        var input_Image = this.refs.inputImage.value

        var dataMovie = {
            title : input_Title, 
            description : input_Description, 
            year : input_Year,
            duration : input_Duration,             
            genre : input_Genre, 
            rating: input_Rating, 
            review : input_Review,
            image_url : input_Image            
        }

        if (input_Title && input_Description && input_Year && input_Duration && input_Genre && input_Rating && input_Review && input_Image) {
            var id = window.location.pathname.split('/')[2]
            Axios.patch(`https://backendexample.sanbersy.com/api/movies/` + id, dataMovie)
            .then((res)=>{
                console.log(res)
                Swal.fire('Update Berhasil')
                window.location = '../movieTable'
                
            })
            .catch((err)=>{
                console.log(err)
            })
            
        }else{
            alert('Data Harus Diisi Semua')
        }


        
    }

    




    render() {
        if (this.state.dataMovie === []) {
            return(
                <Loading />
            )
        }

        return (
            <div className='row justify-content-center'>
                <div className="col-md-4 my-card p-5">
                    <h4>Edit Data Movie</h4>
                    <input className='form-control mt-3'  type='text' placeholder='input title' ref='inputTitle' defaultValue={this.state.dataMovie.title} />
                    <input className='form-control mt-3'  type='text' placeholder='input description' ref='inputDescription' defaultValue={this.state.dataMovie.description}/>
                    <input className='form-control mt-3'  type='text' placeholder='input year' ref='inputYear' defaultValue={this.state.dataMovie.year}/>
                    <input className='form-control mt-3'  type='text' placeholder='input duration' ref='inputDuration' defaultValue={this.state.dataMovie.duration}/>
                    <input className='form-control mt-3'  type='text' placeholder='input genre' ref='inputGenre' defaultValue={this.state.dataMovie.genre}/>
                    <input className='form-control mt-3'  type='text' placeholder='input rating' ref='inputRating' defaultValue={this.state.dataMovie.rating}/>
                    <input className='form-control mt-3'  type='text' placeholder='input review' ref='inputReview' defaultValue={this.state.dataMovie.review}/>
                    <input className='form-control mt-3'  type='text' placeholder='input image url' ref='inputImage' defaultValue={this.state.dataMovie.image_url}/>
                   
                   
                    <input onClick={this.onSaveEditBtnClick}className='btn btn-outline-primary mt-3' value='Save' />
                </div>
            </div>
        );
    }
}

export default editDataMovie;