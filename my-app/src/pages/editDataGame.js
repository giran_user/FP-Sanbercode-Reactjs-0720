import React, { Component } from 'react';
import Axios from 'axios';
import Swal from 'sweetalert2';
import Loading from './loading'

class editDataGame extends Component {
    state = {
        dataGame : []
    }

    componentDidMount = () =>{
        this.getDataGame()
    }

    getDataGame = () => {
        var id = window.location.pathname.split('/')[2]
        Axios.get(`https://backendexample.sanbersy.com/api/games/` + id)
        .then((res)=>{
            this.setState({dataGame : res.data})
            console.log('dapat data')
            console.log(res.data)

        })

        .catch((err)=>{
            console.log(err)

        })
    }

    onSaveEditBtnClick = () =>{
        
        var input_Name = this.refs.inputName.value
        var input_Genre = this.refs.inputGenre.value
        var input_SinglePlayer = this.refs.inputSinglePlayer.value
        var input_MultiPlayer = this.refs.inputMultiPlayer.value
        var input_Platform = this.refs.inputPlatform.value
        var input_Release = this.refs.inputRelease.value
        var input_Image = this.refs.inputImage.value

        var dataGame = {
            name : input_Name,
            genre: input_Genre,
            singlePlayer: input_SinglePlayer,
            multiplayer: input_MultiPlayer,
            platform: input_Platform,
            release: input_Release,
            image_url: input_Image,            
        }

        if (input_Name && input_Genre && input_SinglePlayer && input_MultiPlayer && input_Platform && input_Release && input_Image) {
            var id = window.location.pathname.split('/')[2]
            Axios.patch(`https://backendexample.sanbersy.com/api/games/` + id, dataGame)
            .then((res)=>{
                console.log(res)
                Swal.fire('Update Berhasil')
                window.location = '../gameTable'
                
            })
            .catch((err)=>{
                console.log(err)
            })
            
        }else{
            alert('Data Harus Di isi Semua')
        }


        
    }

    




    render() {
        if (this.state.dataGame === []) {
            return(
                <Loading />
            )
        }

        return (
            <div className='row justify-content-center'>
                <div className="col-md-4 my-card p-5">
                    <h4>Edit Data Game</h4>
                    <input className='form-control mt-3'  type='text' placeholder='input name' ref='inputName' defaultValue={this.state.dataGame.name} />
                    <input className='form-control mt-3'  type='text' placeholder='input genre' ref='inputGenre' defaultValue={this.state.dataGame.genre}/>
                    <input className='form-control mt-3'  type='text' placeholder='input SinglePlayer' ref='inputSinglePlayer' defaultValue={this.state.dataGame.singlePlayer}/>
                    <input className='form-control mt-3'  type='text' placeholder='input MultiPlayer' ref='inputMultiPlayer' defaultValue={this.state.dataGame.multiplayer}/>
                    <input className='form-control mt-3'  type='text' placeholder='input platform' ref='inputPlatform' defaultValue={this.state.dataGame.platform}/>
                    <input className='form-control mt-3'  type='text' placeholder='input release' ref='inputRelease' defaultValue={this.state.dataGame.release}/>
                    <input className='form-control mt-3'  type='text' placeholder='input image url' ref='inputImage' defaultValue={this.state.dataGame.image_url}/>
                   
                   
                    <input onClick={this.onSaveEditBtnClick}className='btn btn-outline-primary mt-3' value='Save' />
                </div>
            </div>
        );
    }
}

export default editDataGame;