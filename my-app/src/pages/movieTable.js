import React, { Component } from 'react';
import Axios from 'axios';
import Loading from './loading';
import { Link } from 'react-router-dom';
import Swal from "sweetalert2"

class movieTable extends Component {
    state = {
        dataMovie : []
    }

    componentDidMount = () =>{
        this.getDataMovie()
    }

    getDataMovie = () => {
        
        Axios.get(`https://backendexample.sanbersy.com/api/movies`)
        .then((res)=>{
            this.setState({dataMovie : res.data})
            console.log(res.data)

        })

        .catch((err)=>{
            console.log(err)

        })
    }

    onDeleteBtnClick = (id,title) =>{
        Swal.fire({
            title: "Delete Data Movie",
            text : "Are You Sure Want to Delete " + title + ' ?',
            showCancelButton: true,
            icon: "warning",
            cancelButtonColor: 'red'

        })
        .then((val)=>{
            if (val.value) {
                Axios.delete(`https://backendexample.sanbersy.com/api/movies/` + id)
                .then((res) => {
                    Swal.fire('Delete Data Success')
                    this.getDataMovie()
                    console.log(res)
                    

                })

                .catch((err) => {
                    console.log(err)

                })                
                
            }else{
                Swal.fire('oke')
            }

        })

    }

    renderDataToJsx = () =>{
        return this.state.dataMovie.map((val,index)=>{
            return(

                <tr>
                    
                                
                    <td>{index + 1}</td>
                    <td>{val.title}</td>
                    <td>{val.description}</td>
                    <td>{val.year}</td>
                    <td>{val.duration}</td>
                    <td>{val.genre}</td>
                    <td>{val.rating}</td>
                    <td>{val.review}</td>
                    <td><img src={val.image_url} width='100px' alt='broken'/></td>
                    <td>
                        <input type='button' class='btn btn-outline-danger' onClick={()=>this.onDeleteBtnClick(val.id,val.title)} value='delete' />

                    </td>

                    <td>
                    <Link to={'/editDataMovie/' + val.id}>
                        <input type='button' class='btn btn-outline-info' value='edit' />
                    </Link>
                    </td>
                </tr>
    
            )

        })
        
    }





    render() {
        if (this.state.dataMovie === []) {
            return(
                <Loading />
            )
        }
        return (
            <div className='container'>
                <h4>Manage Your Movies</h4>
                <div className="table-responsive">

                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Title</th>
                                <th scope="col">Description</th>
                                <th scope="col">Year</th>
                                <th scope="col">Duration</th>
                                <th scope="col">Genre</th>
                                <th scope="col">Rating</th>
                                <th scope="col">Review</th>
                                <th scope="col">Image</th>


                                <th scope="col">Delete</th>
                                <th scope="col">Edit</th>
                            </tr>
                        </thead>
                        <tbody>

                            {this.renderDataToJsx()}
                            
                        </tbody>
                    </table>

                </div>
                
            </div>
        );
    }
}

export default movieTable;