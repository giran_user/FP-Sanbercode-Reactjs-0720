import React, { Component } from 'react';
import Axios from 'axios';
import Loading from './loading';
import { Link } from 'react-router-dom';
import Swal from "sweetalert2"

class gameTable extends Component {
    state = {
        dataGame : []
    }

    componentDidMount = () =>{
        this.getDataGame()
    }

    getDataGame = () => {
        
        Axios.get(`https://backendexample.sanbersy.com/api/games`)
        .then((res)=>{
            this.setState({dataGame : res.data})
            console.log(res.data)

        })

        .catch((err)=>{
            console.log(err)

        })
    }

    onDeleteBtnClick = (id,name) =>{
        Swal.fire({
            title: "Delete Data Game",
            text : "Are You Sure Want to Delete " + name + ' ?',
            showCancelButton: true,
            icon: "warning",
            cancelButtonColor: 'red'

        })
        .then((val)=>{
            if (val.value) {
                Axios.delete(`https://backendexample.sanbersy.com/api/games/` + id)
                .then((res) => {
                    Swal.fire('Delete Data Success')
                    this.getDataGame()
                    console.log(res)
                    

                })

                .catch((err) => {
                    console.log(err)

                })                
                
            }else{
                Swal.fire('oke')
            }

        })

    }

    renderDataToJsx = () =>{
        return this.state.dataGame.map((val,index)=>{
            return(

                <tr>
                    
                                
                    <td>{index + 1}</td>
                    <td>{val.name}</td>
                    <td>{val.genre}</td>
                    <td>{val.singlePlayer}</td>
                    <td>{val.multiplayer}</td>
                    <td>{val.platform}</td>
                    <td>{val.release}</td>
                    <td><img src={val.image_url} width='100px' alt='broken'/></td>
                    <td>
                        <input type='button' class='btn btn-outline-danger' onClick={()=>this.onDeleteBtnClick(val.id,val.name)} value='delete' />

                    </td>

                    <td>
                    <Link to={'/editDataGame/' + val.id}>
                        <input type='button' class='btn btn-outline-info' value='edit' />
                    </Link>
                    </td>
                </tr>
    
            )

        })
        
    }





    render() {
        if (this.state.dataGame === []) {
            return(
                <Loading />
            )
        }
        return (
            <div className='container'>
                <h4>Manage Your Games</h4>
                <div className="table-responsive">

                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Name</th>
                                <th scope="col">Genre</th>
                                <th scope="col">SinglePlayer</th>
                                <th scope="col">MultiPlayer</th>
                                <th scope="col">Platform</th>
                                <th scope="col">Release</th>
                                <th scope="col">Image</th>


                                <th scope="col">Delete</th>
                                <th scope="col">Edit</th>
                            </tr>
                        </thead>
                        <tbody>

                            {this.renderDataToJsx()}
                            
                        </tbody>
                    </table>

                </div>
                
            </div>
        );
    }
}

export default gameTable;